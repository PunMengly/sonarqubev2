#!/bin/bash

# Loop from 2 to 12
for i in {2..12}; do
    # Create folder name with leading zeros if necessary
    old_folder="LAB$(printf "%02d" $i)"
    new_folder="LAB$(printf "%03d" $i)"
    
    # Rename folder
    if [ -d "$old_folder" ]; then
        mv "$old_folder" "$new_folder"
        echo "Renamed $old_folder to $new_folder"
    else
        echo "$old_folder does not exist"
    fi
done